import logging
from uuid import getnode as get_mac

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
from src.settings import *

LOG_FILENAME = '../jammer.log'
logging.basicConfig(filename=LOG_FILENAME, level=logging.INFO)


def login(user_name, password):
    driver.get(login_url)
    driver.find_element_by_class_name("sign-in").click()
    driver.find_element_by_id("signin-login-field").send_keys(user_name)
    driver.find_element_by_id("signin-password-field").send_keys(password)
    driver.find_element_by_xpath("//input[@value='Sign in']").click()


def navigate(href):
    driver.find_element_by_xpath("//a[@href='" + href + "']").click()


def navigate_to_tab(choice):
    return {
        'trending': navigate('/trending')
    }[choice.lower()]


def get_price_choice(choice):
    return {
        '$': 'low',
        '$$': 'medium',
        '$$$': 'high'
    }[choice.lower()]


def decide_to_click(ele, choice_arr):
    inner_text = str(ele.text).lower()

    if str(ele.get_attribute("class")).__eq__(''):
        class_val = ""
    else:
        class_val = str(ele.get_attribute("class")).lower()

    if (inner_text in choice_arr and class_val is None) or (
                    inner_text not in choice_arr and class_val.__eq__("active")):
        ele.click()


def choose_settings(category, price):
    category_setting_ele = driver.find_element_by_id('category-settings')
    product_category_ele = category_setting_ele.find_element_by_class_name('product-category-btns')
    price_category_ele = category_setting_ele.find_element_by_class_name('price-category-btns')

    for ele in product_category_ele.find_elements_by_xpath(".//*"):
        decide_to_click(ele, category)

    for ele in price_category_ele.find_elements_by_xpath(".//*"):
        decide_to_click(ele, price)


def choose_settings_from_url(category, price):
    category_url = '-'.join([str(x) for x in category])
    price_url = list()

    for p in price:
        price_url.append(get_price_choice(p))

    price_str = '-'.join([str(x) for x in price_url])

    driver.get(login_url + 'trending?c=' + category_url + '&p=' + price_str)


def get_user_names(limit):
    trending_product_cards = driver.find_elements_by_xpath(
        "//div[@class='product-card product-overlay-container generic-product-card']")
    cards_link_list = set()
    cards_savers_link_list = set()
    username_list = set()
    username_list_len = 0

    for ele in trending_product_cards:
        cards_link_list.add(str(ele.find_elements_by_tag_name('a')[0].get_attribute('href')))

    for card in cards_link_list:
        driver.get(card)
        savers_ele = driver.find_element_by_class_name('products-show-more-savers-text')
        anchors = savers_ele.find_elements_by_tag_name('a')
        if anchors:
            link = str(anchors[0].get_attribute('href'))
            cards_savers_link_list.add(link)
            driver.get(link)
            description_ele = driver.find_elements_by_class_name('preview-strip-description-text')
            for desc in description_ele:
                username = str(desc.find_element_by_tag_name('a').text)
                username_list.add(username)
                username_list_len += 1

                if username_list_len >= limit:
                    return username_list


def open_profile_stories(name):
    driver.get(login_url + name + '/stories')


def post_usernames_to_story(names, choice):
    stories = driver.find_elements_by_class_name("submit-comment-textarea-container")
    for ch in choice:
        for name in names:
            stories[ch-1].find_element_by_tag_name("textarea").send_keys('@'+name)
            # stories[ch-1].find_element_by_tag_name("textarea").send_keys(Keys.ENTER)
            stories[ch-1].find_element_by_tag_name("textarea").clear()


def post_usernames_to_stories(names, choice, profiles):
    for p in profiles:
        open_profile_stories(p)
        post_usernames_to_story(names, choice)


def split_string(line):
    line = line.replace(' ', '')
    return line.split(',')


if __name__ == '__main__':
    driver = webdriver.Chrome(chrome_driver)
    driver.maximize_window()
    mac = get_mac()

    try:
        username_file = open('username.txt', 'r+')
        credentials = open('credentials.txt', 'r+')
        category_choice = open('category_choice.txt', 'r+')
        price_choice = open('price_choice.txt', 'r+')
        limit = open('limit.txt', 'r+')
        profile_names = open('profile_names.txt', 'r+')
        black_list_file = open('black_list.txt', 'r+')
        stories_choice_file = open('stories_choice.txt', 'r+')
        start = open('start_up.log', 'r+')
        s = start.readline()
        if str(s) == 'started at:':
            start.write(str(mac))
        elif str(s) == 'started at:' + str(mac):
            print "sdf"
        else:
            exit()

        cred = split_string(credentials.readline())
        cat = split_string(category_choice.readline())
        pri = split_string(price_choice.readline())
        profile = split_string(profile_names.readline())
        limit_ = int(limit.readline())
        black_list = split_string(black_list_file.readline())
        stories_choice = [int(x) for x in split_string(stories_choice_file.readline())]

        login(cred[0], cred[1])
        choose_settings_from_url(cat, pri)
        username_list = get_user_names(limit_)
        existing_username_list = username_file.readline()
        existing_username_list = existing_username_list.split(',')

        new_username_list = set(username_list) - set(existing_username_list)
        new_username_list = set(new_username_list) - set(black_list)

        post_usernames_to_stories(new_username_list, stories_choice, profile)
        username_file.write(','.join([str(x) for x in new_username_list]))
    except NoSuchElementException, e:
        logging.info("NoSuchElementException: ", e)
    except IOError, e:
        logging.error("IOError: ", e)
    except MemoryError, e:
        logging.error("MemoryError: ", e)
    except IndexError, e:
        logging.error("IndexError: ", e)
    finally:
        username_file.close()
        credentials.close()
        category_choice.close()
        price_choice.close()
        limit.close()
        profile_names.close()
        black_list_file.close()
        start.close()
        driver.close()
